package TestsFor::Acme::SecretSanta::Participant;

use Try::Tiny;
use Test::Class::Moose;
with 'Test::Class::Moose::Role::AutoUse';

sub test_use : Tests {
  my $test = shift;
  use_ok $test->class_name;

  my $t = try { $test->class_name->new } catch { $_ };
  like $t, qr/required/i, 'Constructor fails with no name';

  ok $test->class_name->new( name => 'test' ),
    'Constructor suceeds with hash';

  ok my $self = $test->class_name->new({ name => 'test' }),
    'Constructor suceeds with hashref';

  ok exists $self->exceptions->{test}, 'Participant automatically excludes self';
}

sub test_attribute : Tests {
  my $test = shift;

  foreach (qw( name address language exceptions groups target )) {
    can_ok $test->class_name, $_;
  }
}

sub test_target : Tests {
  my $test = shift;

  my $self = $test->class_name->new( name => 'foo' );
  $self->target($test->class_name->new( name => 'bar' ));

  isa_ok $self->reset, $test->class_name, 'Reset returns self';
  ok !defined $self->target, 'Reset clears target';
}

sub test_method : Tests {
  my $test = shift;

  can_ok $test->class_name, $_
    foreach qw( join_group leave_group add_exceptions remove_exceptions reset );
}

sub test_groups : Tests {
  my $test = shift;
  my $self = $test->class_name->new( name => 'foo' );

  is $self->total_groups, 0, 'Participant starts with no groups';

  require Acme::SecretSanta::Group;
  my $a = Acme::SecretSanta::Group->new( name => 'a' );
  my $b = Acme::SecretSanta::Group->new( name => 'b' );

  $self->join_group( $a );
  is $self->total_groups, 1, 'Participant can join groups';

  $self->join_group( $a );
  is $self->total_groups, 1, 'Joining groups is idempotent';

  $self->leave_group( $a );
  is $self->total_groups, 0, 'Participant can leave groups';

  $self->leave_group( $a );
  is $self->total_groups, 0, 'Leaving groups is idempotent';

  $self->join_group( $a, $b );
  is $self->total_groups, 2, 'Participant can join multiple groups';

  $self->leave_group( $a, $b );
  is $self->total_groups, 0, 'Participant can leave multiple groups';
}

q{All's well that ends well};
