package TestsFor::Acme::SecretSanta::Renderer::Mason;

use Try::Tiny;
use Test::Class::Moose;
with 'Test::Class::Moose::Role::AutoUse';

use Acme::SecretSanta::Participant;

sub test_use : Tests {
  my $test = shift;
  use_ok $test->class_name;

  ok my $self = $test->class_name->new, 'Constructor suceeds';
  ok $self->does('Acme::SecretSanta::Role::Renderer'), 'Renderer does role';
}

sub test_render {
  my $test = shift;
  my $self = $test->class_name->new;

  my $email;

  my $foo = Acme::SecretSanta::Participant->new(
    name => 'foo', address => 'Foosome', language => 'es',
  );
  my $bar = Acme::SecretSanta::Participant->new(
    name => 'bar', address => 'Barton',
  );

  $foo->target($bar);
  $bar->target($foo);

  $email = $self->render($foo);
  isa_ok $email, 'Email::Simple';

  like $email->body, qr/te tocó bar/i, 'Renders template in Spanish';

  $email = $self->render($bar);
  like $email->body, qr/you got foo/i, 'Renders template in English';

}

q{All's well that ends well};
