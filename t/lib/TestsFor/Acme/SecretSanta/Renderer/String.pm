package TestsFor::Acme::SecretSanta::Renderer::String;

use Try::Tiny;
use Test::Class::Moose;
with 'Test::Class::Moose::Role::AutoUse';

use Acme::SecretSanta::Participant;

sub test_use : Tests {
  my $test = shift;
  use_ok $test->class_name;

  ok my $self = $test->class_name->new, 'Constructor suceeds';
  ok $self->does('Acme::SecretSanta::Role::Renderer'), 'Renderer does role';
}

sub test_render {
  my $test = shift;
  my $self = $test->class_name->new;

  my $foo = Acme::SecretSanta::Participant->new(
    name => 'foo', address => 'Foosome',
  );
  my $bar = Acme::SecretSanta::Participant->new(
    name => 'bar', address => 'Barton',
  );

  $foo->target($bar);

  like $self->render($foo), qr/foo -> bar/i, 'Renders as string';

  $self->format('%N <- %n');
  like $self->render($foo), qr/bar <- foo/i, 'Can reformat string';

  $self->format('%a -> %A');
  like $self->render($foo), qr/Foosome -> Barton/i, 'Format string can access address';

  $self->format('%n %s to %N');
  like $self->render($foo, 'gives'), qr/gives to/i, 'Formatter also uses sprintf';

}

q{All's well that ends well};
