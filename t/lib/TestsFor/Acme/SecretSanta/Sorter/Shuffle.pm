package TestsFor::Acme::SecretSanta::Sorter::Shuffle;

use Try::Tiny;
use Test::Class::Moose;
with 'Test::Class::Moose::Role::AutoUse';

sub test_use : Tests {
  my $test = shift;
  use_ok $test->class_name;

  ok $test->class_name->new, 'Constructor suceeds';
}

sub test_role : Tests {
  my $test = shift;

  ok $test->class_name->does('Acme::SecretSanta::Role::Sorter'),
    'Shuffle sorter does sorter role';
}

q{All's well that ends well};
