package TestsFor::Acme::SecretSanta::Dispatcher::Handle;

use Try::Tiny;
use Test::Class::Moose;
with 'Test::Class::Moose::Role::AutoUse';

use Acme::SecretSanta::Participant;
use Capture::Tiny ':all';

sub test_use : Tests {
  my $test = shift;
  use_ok $test->class_name;

  ok my $self = $test->class_name->new, 'Constructor suceeds';
  ok $self->does('Acme::SecretSanta::Role::Dispatcher'), 'Renderer does role';
}

sub test_attribute {
  my $test = shift;
  my $self = $test->class_name->new;

  ok $self->can($_) foreach qw( output );
}

sub test_method {
  my $test = shift;
  my $self = $test->class_name->new;

  ok $self->can($_) foreach qw( dispatch );
}

sub test_render {
  my $test = shift;
  my $self = $test->class_name->new;

  my ($stdout, $stderr);

  ($stdout, $stderr) = capture { $self->dispatch( 'Test' ) };
  like $stdout, qr/test/i, 'Screen dispatcher prints to STDOUT';

  $self->output( \*STDERR );
  ($stdout, $stderr) = capture { $self->dispatch( 'Test' ) };
  like $stderr, qr/test/i, 'Screen dispatcher prints to STDERR';

}

q{All's well that ends well};
