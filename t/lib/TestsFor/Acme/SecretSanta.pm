package TestsFor::Acme::SecretSanta;

use Try::Tiny;
use Test::Class::Moose;
with 'Test::Class::Moose::Role::AutoUse';

use File::Share ':all';
use Path::Tiny qw( path );
use JSON::MaybeXS qw( decode_json );
use Acme::SecretSanta::Renderer::Mason;
use Capture::Tiny ':all';

sub test_use : Tests {
  my $test = shift;
  use_ok $test->class_name;

  ok $test->class_name->new, 'Constructor suceeds';
}

sub test_attribute : Tests {
  my $test = shift;

  can_ok $test->class_name, $_ foreach qw(
    groups dispatcher sorter renderer participants is_sorted
  );

  my $self = $test->class_name->new;
  is !!($self->is_sorted), !!(0), 'Santa starts unsorted';
  is !!($self->sorter->needs_retry), !!(0), 'Sorter does not need retry';
}

sub test_method : Tests {
  my $test = shift;

  can_ok $test->class_name, $_ foreach qw(
    total_participants participants remove_participant
    add_participant dispatch render sort load
  );
}

sub test_participants : Tests {
  my $test = shift;
  use_ok $test->class_name;

  my $self = $test->class_name->new;
  is $self->total_participants, 0, 'Participants default to empty';

  require Acme::SecretSanta::Participant;

  $self->add_participant( Acme::SecretSanta::Participant->new( name => 'foo' ) );
  is $self->total_participants, 1, 'Added one participant';
  isa_ok $self->get_participant('foo'), 'Acme::SecretSanta::Participant';

  $self->add_participant('bar');
  is $self->total_participants, 2, 'Added one participant from string';
  isa_ok $self->get_participant('bar'), 'Acme::SecretSanta::Participant';

  $self->remove_participant('foo');
  is $self->get_participant('foo'), undef, 'Removed participant';

  $self->add_participant('foo', 'baz');
  is $self->total_participants, 3, 'Added list of participants';

  $self->remove_participant('bar', 'baz');
  is $self->total_participants, 1, 'Removed list of participants';
}

sub test_sorter : Tests {
  my $test = shift;
  use_ok $test->class_name;

  my $x;

  require Acme::SecretSanta::Participant;
  my $self = $test->class_name->new;
  my $sorter = $self->sorter;

  $self->add_participant('foo');

  $x = try { $self->sort; 0 } catch { $_ };
  like $x, qr/not enough participants/i, 'sort fails with too few participants';

  $self->add_participant('bar', 'baz');
  ok $self->sort, 'sort succeeds with three or more participants';

  is !!($self->is_sorted), !!(1), 'Santa is_sorted after sorting';

  $x = try { $self->sort; 0 } catch { $_ };
  like $x, qr/already sorted/i, 'sort fails if already sorted';

  ok $sorter->reset, 'reset succeeds';

  is !!($self->is_sorted), !!(0), 'Santa is not sorted after resetting';

  isa_ok $sorter->reset, ref $sorter;

  my $tries = 0;
  $self->sorter->once( assign => sub {
    ok $_[0]->retry, 'retry succeeds';
    is !!($_[0]->needs_retry), !!(1), 'Sorter needs retry after calling retry';
  });

  $self->sorter->on( sort => sub { $tries++ });

  $sorter->sort;

  is $tries, 1, 'Retried once';

  $self->add_participant( 'zoo' );

  is !!($self->is_sorted), !!(0), 'Santa is not sorted after adding participants';

  $sorter->sort;

  $self->remove_participant( 'zoo' );

  is !!($self->is_sorted), !!(0), 'Santa is not sorted after removing participants';

  my ($foo, $bar, $baz) = $self->participants;

  $x = try { $sorter->assign_target } catch { $_ };
  like $x, qr/not enough arguments/i, 'Cannot assign target without arguments';

  $x = try { $sorter->assign_target($foo, undef) } catch { $_ };
  like $x, qr/target is not defined/i, 'Cannot assign target without defined target';

  $x = try { $sorter->assign_target(undef, $bar) } catch { $_ };
  like $x, qr/source is not defined/i, 'Cannot assign target without defined source';

  ok $sorter->assign_target($foo, $bar), 'Assign target suceeds';
  is $foo->target, $bar, 'Target properly assigned';

  $x = try { $sorter->assign_target($foo, $baz) } catch { $_ };
  like $x, qr/already has a target/i, 'Cannot assign target if source already has target';

  $sorter->reset;

  ok !defined $foo->target, 'Resetting sorter clears assigned targets';

  $foo->add_exceptions( $baz );

  $x = try { $sorter->assign_target($foo, $baz) } catch { $_ };
  like $x, qr/in source exception list/i, 'Cannot assign target if it is in exception list';

  $sorter->reset;

  $sorter->assign_target($foo, $bar);
  $sorter->assign_target($bar, $foo);
  is !!($sorter->needs_retry), !!(0), 'Can assign crossed participants';

  $sorter->reset;
  $sorter->on( assign => $sorter->no_crossings );

  $sorter->assign_target($foo, $bar);
  $sorter->assign_target($bar, $foo);
  is !!($sorter->needs_retry), !!(1), 'Can disable assignment of crossed participants';

  is scalar(@{$sorter->subscribers( 'assign' )}), 1, 'Has subscribers';
  $sorter->reset;
  is scalar(@{$sorter->subscribers( 'assign' )}), 1, 'Reset does not clear subscribers';
  $sorter->clear_subscribers;
  is scalar(@{$sorter->subscribers( 'assign' )}), 0, 'Can clear subscribers';

  my ($zoo) = $self->add_participant( 'zoo' );

  $self->set_group( 'a' => $foo, $bar );
  $self->set_group( 'b' => $baz, $zoo );

  $sorter->on( assign => $sorter->no_group_intersections );

  $sorter->assign_target($foo, $bar);
  $sorter->assign_target($zoo, $baz);
  is !!($sorter->needs_retry), !!(0), 'Can assign without group intersections';

  $sorter->reset;

  $sorter->assign_target($foo, $zoo);
  $sorter->assign_target($bar, $baz);
  is !!($sorter->needs_retry), !!(1), 'Can detect group intersections';

}

sub test_load : Tests {
  my $test = shift;

  my ($self, $x);

  my $file = path(dist_dir 'Acme-SecretSanta')->child('data/test.json');
  my $data = decode_json $file->slurp;

  $x = try { $test->class_name->load } catch { $_ };
  like $x, qr/not enough arguments/i, 'Load dies when not given enough arguments';

  ok $self = $test->class_name->load( $file->canonpath ), 'Loader succeeds with filename';
  is $self->total_participants, 6, 'Loaded correct participants';

  ok $self = $test->class_name->load( $data ), 'Loader succeeds with arrayref';
  is $self->total_participants, 6, 'Loaded correct participants';

  # Remove exceptions, because otherwise
  # they'll make reference to non-existing participants
  delete $data->[0]->{exceptions};

  ok $self = $test->class_name->load( $data->[0] ), 'Loader succeeds with single hashref';
  is $self->total_participants, 1, 'Loaded correct participant';

  my $renderer = Acme::SecretSanta::Renderer::Mason->new;
  ok $self = $test->class_name->load( $data, { renderer => $renderer } ), 'Loader succeeds with additional data';
  isa_ok $self->renderer, 'Acme::SecretSanta::Renderer::Mason', 'Can set additional fields';
  is $self->renderer->santa, $self, 'Additional fields properly linked';
}

sub test_groups : Tests {
  my $test = shift;

  my $self = $test->class_name->new;
  my ($foo, $bar, $baz) = $self->add_participant(qw( foo bar baz ));

  is $self->total_groups, 0, 'Santa starts with no groups';

  my $group = $self->set_group( 'a' => $foo );
  is $self->total_groups, 1, 'Groups are created from participant methods';

  ok $group->is_member( $foo ), 'Foo is member of group';
  ok !$group->is_member( $bar ), 'Bar is not member of group';

  $foo->leave_group( $group );
  is $self->total_groups, 1, 'There can be empty groups';

  $group->add_member( $foo, $bar );
  is $group->total_members, 2, 'Can get group members';

  $foo->leave_group( $group );

  my ($a) = $self->get_group('a')->members;
  is $a, $bar, 'Group members returns participants';

}

sub test_pipeline : Tests {
  my $test = shift;

  my $file = path(dist_dir 'Acme-SecretSanta')->child('data/test.json');
  my $self = $test->class_name->load( $file->canonpath );

  my $x;

  $x = try { $self->renderer->render_summary } catch { $_ };
  like $x, qr/unsorted santa object/i, 'Cannot summarise if not sorted';

  $self->sort;

  my $summary = $self->renderer->render_summary;
  is scalar(split(/\n/, $summary)), $self->total_participants,
    'String summary has resulst for all participants';

  my @participants = $self->participants;
  like my $msg = $self->render( $participants[1] ), qr/\w+ -> \w+/i, 'Can render through shortcut';

  my ($stdout, $stderr) = capture { $self->dispatch( $msg ) };
  like $stdout, qr/\w+ -> \w+/i, 'Can dispatch through shortcut';
}

q{All's well that ends well};
