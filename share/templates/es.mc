<%class>
has 'person';
has 'time';
</%class>

Este es un correo generado automágicamente para que sepas
quién te tocó para el amigo secreto de este año.

Así que felicitaciones, <% $.person->name %>: ¡te tocó <% $.person->target->name %>!

¡Feliz Navidad!

Este correo se envió el <% $.time %>
