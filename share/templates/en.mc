<%class>
has 'person';
has 'time';
</%class>

This is an automated email to let you know the result
of this year's Secret Santa draw.

So congratulations, <% $.person->name %>: you got <% $.person->target->name %>!

Merry Christmas!

This email was sent on <% $.time %>
