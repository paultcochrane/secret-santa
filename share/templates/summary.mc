<%class>
has 'santa';
has 'time';
</%class>

This is an automagically generated email to serve as backup
for the results of this year's Secret Santa draw.

Needless to say, the information in this email is _secret_
and should be kept this way. You are not to disclose any of the
information on this email to any of the participants, unless
they happen to forget or lose track of who they got assigned to.
In that case, of course, you can only inform them of the person
_they_ got.

Thank you for doing this!

This year's results are:

% foreach my $person ($.santa->list_participants) {
  <% $person->name %> gives to <% $person->target->name %>
% }

Merry Christmas!

This email was sent on <% $.time %>
