use strict;
use warnings;

package Acme::SecretSanta::Role::Participant;

our $VERSION = '0';

use Moo::Role;

with 'Acme::SecretSanta::Role::Base';

use MooX::HandlesVia;
use MooX::late;

use Types::Standard qw( HashRef ArrayRef Undef Str );
use Acme::SecretSanta::Types qw( SantaParticipant SantaGroup );
use List::Util qw( any );
use Scalar::Util qw( weaken );
use Ref::Util qw( is_blessed_ref );
use Carp;

has name => (
  is => 'ro',
  required => 1,
  isa => Str,
);

has address => (
  is => 'rw',
  isa => Str|Undef,
);

has language => (
  is => 'rw',
  lazy => 1,
  isa => Str,
  default => 'en',
);

has target => (
  is => 'rw',
  isa => SantaParticipant|Undef
);

has exceptions => (
  is => 'ro',
  coerce => 1,
  isa => HashRef[SantaParticipant],
  lazy => 1,
  default => sub { { } },
  handles_via => 'Hash',
  handles => {
    add_exceptions => 'set',
    remove_exceptions => 'delete',
    total_exceptions => 'count',
    list_exceptions => 'keys',
  },
);

has _groups => (
  is => 'ro',
  coerce => 1,
  isa => HashRef[SantaGroup],
  lazy => 1,
  default => sub { {} },
  handles_via => 'Hash',
  handles => {
    join_group   => 'set',
    leave_group  => 'delete',
    groups       => 'values',
    total_groups => 'count',
  },
);

around join_group => sub {
  my $orig = shift;
  my $self = shift;

  croak 'Cannot add member: need a list of references'
    if any { !is_blessed_ref($_) } @_;

  foreach (@_) {
    $self->$orig( $_->name => $_ );
    weaken($self->{_groups}{$_->name});
  }

  return $self;
};

around leave_group => sub {
  my $orig = shift;
  my $self = shift;

  foreach (@_) {
    $_->remove_member( $self );
    $self->$orig($_->name);
  }

  return $self;
};

around add_exceptions => sub {
  my $orig = shift;
  my $self = shift;
  foreach (@_) {
    $self->$orig( $_->name => $_ );
    weaken($self->{exceptions}{$_->name});
  }
  return $self->$orig;
};

around remove_exceptions => sub {
  my $orig = shift;
  my $self = shift;
  $self->$orig( map { $_->name } @_ );
};

foreach my $method (
    qw( join_group leave_group add_exceptions remove_exceptions )
  ) {

  around $method => sub {
    my $orig = shift;
    my $self = shift;
    $self->$orig( @_ );
    return $self;
  };
}

around reset => sub {
  my $orig = shift;
  my $self = shift;
  $self->target(undef);
  return $self->$orig(@_);
};

sub BUILD {
  my ($self, $args) = @_;
  $self->add_exceptions($self);
}

q{Let it snow, let it snow, let it snow};
