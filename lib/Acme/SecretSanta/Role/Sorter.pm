use strict;
use warnings;

package Acme::SecretSanta::Role::Sorter;

our $VERSION = '0';

use Moo::Role;
use MooX::late;

use Types::Standard qw( HashRef );
use Acme::SecretSanta::Types qw( SantaParticipant );
use Carp;

with 'Acme::SecretSanta::Role::Base';
with 'Role::EventEmitter';

# The sort method should return self, to allow for chaining
requires 'sort';

before sort => sub {
  $_[0]->emit('sort');
};

has _retry => (
  is => 'rw',
  default => 0,
);
sub retry { $_[0]->_retry(1) }
sub needs_retry { $_[0]->_retry }

has todo => (
  is => 'rw',
  isa => HashRef[SantaParticipant],
);

around reset => sub {
  my $orig = shift;
  my $self = shift;

  $self->_retry(0);
  $self->santa->is_sorted(0);

  $self->todo( { map { $_->name => $_ } $self->santa->participants } );

  $_->reset foreach $self->santa->participants;

  return $self->$orig(@_);
};

sub clear_subscribers {
  my ($self) = shift;
  $self->unsubscribe($_) foreach qw( sort assign );
}

sub assign_target {
  my ($self, $source, $target) = @_;

  croak 'Not enough arguments to assign'
    unless scalar @_ == 3;

  croak 'Source is not defined'
    unless defined $source;

  croak 'Target is not defined'
    unless defined $target;

  croak 'Source already has a target'
    if defined $source->target;

  croak 'Target is in source exception list'
    if defined $source->exceptions->{$target->name};

  $source->target( delete $self->todo->{$target->name} );
  $self->emit( assign => $source );

  return 1;
}

sub no_group_intersections {
  return sub {
    my ($sorter, $source) = @_;

    use Array::Utils qw( intersect );
    foreach my $group ($source->groups) {

      foreach my $team_mate ($group->members) {
        next if $team_mate->name eq $source->name;

        if (defined $team_mate->target) {
          my @source_groups    = $source->target->groups;
          my @team_mate_groups = $team_mate->target->groups;

          return $sorter->retry
            if intersect (@source_groups, @team_mate_groups);
        }
      }
    }
  };
}

sub no_crossings {
  return sub {
    my ($sorter, $source) = @_;
    if (defined $source->target->target) {
      return $sorter->retry if $source->target->target eq $source;
    }
  };
}

q{I saw mommy kissing Santa Claus};
