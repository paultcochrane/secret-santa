use strict;
use warnings;

package Acme::SecretSanta::Role::Dispatcher;

our $VERSION = '0';

use Moo::Role;

with 'Acme::SecretSanta::Role::Base';

requires 'dispatch';

q{Last Christmas I gave you my heart};
