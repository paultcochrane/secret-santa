use strict;
use warnings;

package Acme::SecretSanta::Role::Renderer;

our $VERSION = '0';

use Moo::Role;
use Carp;

with 'Acme::SecretSanta::Role::Base';

requires 'render';
requires 'render_summary';

before render_summary => sub {
  my ($self) = @_;

  croak 'Cannot render summary without a defined santa'
    unless defined $self->santa;

  croak 'Cannot create summary of an unsorted santa object'
    unless $self->santa->is_sorted;
};

before render => sub {
  my ($self, $participant) = @_;

  croak 'Cannot render without a participant'
    unless defined $participant;

  croak 'Cannot render a participant without a target'
    unless defined $participant->target;
};

q{Let it snow, let it snow, let it snow};
