use strict;
use warnings;

package Acme::SecretSanta::Role::Base;

our $VERSION = '0';

use Moo::Role;

has santa => ( is => 'rw', weak_ref => 1 );

sub reset { return shift }

sub BUILDARGS {
  my $self = shift;
  my $args = (@_) ? (@_ > 1) ? { @_ } : shift : {};

  return $args;
}

q{Let it snow, let it snow, let it snow};
