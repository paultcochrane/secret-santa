use strict;
use warnings;

package Acme::SecretSanta::Types;

our $VERSION = '0';

use Type::Library -base;

use Type::Utils -all;
use Types::Standard qw();

role_type 'SantaSorter',      { role => 'Acme::SecretSanta::Role::Sorter' };

role_type 'SantaRenderer',    { role => 'Acme::SecretSanta::Role::Renderer' };

role_type 'SantaDispatcher',  { role => 'Acme::SecretSanta::Role::Dispatcher' };

role_type 'SantaParticipant', { role => 'Acme::SecretSanta::Role::Participant' };

class_type 'SantaGroup',      { class => 'Acme::SecretSanta::Group' };

q{Let it snow, let it snow, let it snow};
