use strict;
use warnings;

package Acme::SecretSanta::Sorter::Shuffle;

our $VERSION = '0';

use Moo;

with 'Acme::SecretSanta::Role::Sorter';

use List::Util qw( shuffle );
use Carp;

has _failsafe => (
  is => 'ro',
  lazy => 1,
  default => 100,
);

sub sort {
  my ($self) = @_;

  croak 'Need a parent SecretSanta object' unless defined $self->santa;

  croak 'Already sorted' if $self->santa->is_sorted;

  croak 'Not enough participants' unless $self->santa->total_participants > 2;

  my $failsafe = 0;

  SORT: while (!$self->santa->is_sorted and $failsafe < $self->_failsafe) {
    $failsafe++;

    $self->reset;

    foreach my $source ($self->santa->participants) {
      my %candidates =
        map { $_ => $self->santa->get_participant($_) }
        grep { !exists $source->exceptions->{$_} }
        keys %{$self->todo};

      my @candidates = keys %candidates;
      last unless scalar @candidates;

      if (scalar @candidates > 1) {
        my @candidates = shuffle @candidates;
      }

      my $target = shift @candidates;
      last unless $self->assign_target($source, $candidates{$target});

      if ($self->needs_retry) {
        next SORT;
      }
    }

    $self->santa->is_sorted(1) unless scalar keys %{$self->todo};
  }

  croak 'Could not sort participants after ', $failsafe, ' attempts. Aborting!'
    unless $self->santa->is_sorted;

  return $self;
}

q{In my Christmas jammies};
