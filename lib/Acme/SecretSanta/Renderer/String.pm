use strict;
use warnings;

package Acme::SecretSanta::Renderer::String;

our $VERSION = '0';

use Moo;
use String::Format;

use Carp;

with 'Acme::SecretSanta::Role::Renderer';

has format => (
  is => 'rw',
  default => "[ %n -> %N ]\n",
);

sub render_summary {
  my ($self) = @_;

  my $out = q{};
  $out .= $self->render($_) foreach $self->santa->participants;

  return $out;
}

sub render {
  my ($self, $person, @rest) = @_;

  my %source = (
    n => $person->name,
    a => $person->address,
    N => $person->target->name,
    A => $person->target->address,
  );

  return sprintf stringf($self->format, %source), @rest;
}

q{It's beginning to look a lot like Christmas};
