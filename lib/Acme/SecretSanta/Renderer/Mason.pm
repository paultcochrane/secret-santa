use strict;
use warnings;

package Acme::SecretSanta::Renderer::Mason;

our $VERSION = '0';

use Moo;
use Mason;
use File::Share qw( dist_dir );
use Path::Tiny qw( path );
use Carp;

with 'Acme::SecretSanta::Role::Renderer';

has mason    => ( is => 'ro' );
has from     => ( is => 'ro' );
has subject  => ( is => 'ro' );
has username => ( is => 'ro' );
has password => ( is => 'ro' );

around BUILDARGS => sub {
  my $orig = shift;
  my $self = shift;
  my $args = (@_) ? (@_ > 1) ? { @_ } : shift : {};

  if (ref $args->{mason} ne 'Mason') {
    $args->{mason}{comp_root} //=
      path(dist_dir 'Acme-SecretSanta')->child('templates')->canonpath;
    $args->{mason} = Mason->new(%{$args->{mason}});
  }

  return $self->$orig($args);
};

sub render_summary {
  my ($self, $address) = @_;

  croak 'Cannot render summary as email without a TO address'
    unless defined $address;

  my $body = $self->mason->run(
    '/summary',
    santa => $self->santa,
    time => strftime('%a, %d %b %Y %T %z', localtime),
  )->output;

  return $self->_make_email($body, $address);
}

sub render {
  my ($self, $person) = @_;

  my $body = $self->mason->run(
    '/' . $person->language,
    person => $person,
    time => strftime('%a, %d %b %Y %T %z', localtime),
  )->output;

  return $self->_make_email($body, $person->address);
}

sub _make_email {
  my ($self, $data, $to) = @_;

  use Email::Simple;
  use POSIX;

  return Email::Simple->create(
    header => [
      From => $self->from // '',
      Subject => $self->subject,
      To => $to // '',
    ],
    body => $data,
  );
}

q{'Twas nigh afore Christmas at the Freemason's Hall};
