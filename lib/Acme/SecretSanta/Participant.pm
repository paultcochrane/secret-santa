use strict;
use warnings;

package Acme::SecretSanta::Participant;

our $VERSION = '0';

use Moo;

with 'Acme::SecretSanta::Role::Participant';

q{Have yourself a Merry Little Christmas};
