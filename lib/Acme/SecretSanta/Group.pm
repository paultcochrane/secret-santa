use strict;
use warnings;

package Acme::SecretSanta::Group;

our $VERSION = '0';

use Moo;

with 'Acme::SecretSanta::Role::Base';

use MooX::HandlesVia;
use MooX::late;

use Types::Standard qw( HashRef Str );
use Acme::SecretSanta::Types qw( SantaParticipant );
use Scalar::Util qw( weaken );
use Ref::Util qw( is_blessed_ref );
use Carp;

has name => (
  is => 'ro',
  isa => Str,
  required => 1,
);

has _members => (
  is => 'ro',
  isa => HashRef[SantaParticipant],
  lazy => 1,
  default => sub { { } },
  handles_via => 'Hash',
  handles => {
    add_member => 'set',
    remove_member => 'delete',
    total_members => 'count',
    members => 'values',
  },
);

around add_member => sub {
  my $orig = shift;
  my $self = shift;

  foreach (@_) {
    croak 'Cannot add member: not a reference'
      unless is_blessed_ref($_);

    $self->$orig($_->name, $_);
    weaken($self->{_members}{$_->name});

    $_->join_group($self);
  }

  return $self;
};

around remove_member => sub {
  my $orig = shift;
  my $self = shift;
  $self->$orig($_->name) foreach @_;
  return $self;
};

sub is_member {
  my ($self, $participant) = @_;
  return defined $self->_members->{$participant->name};
}

q{Let it snow, let it snow, let it snow};
