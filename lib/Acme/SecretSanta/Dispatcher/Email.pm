use strict;
use warnings;

package Acme::SecretSanta::Dispatcher::Email;

our $VERSION = '0';

use Moo;

with 'Acme::SecretSanta::Role::Dispatcher';

use Net::SMTP;
use Encode qw( encode_utf8 );

has [qw( port server username password )] => ( is => 'ro' );

sub dispatch {
  my ($self, $mail) = @_;
  my $to   = $mail->header('To');
  my $from = $mail->header('From');

  print 'Sending email notification to ', $to, "\n";

  my $smtp = Net::SMTP->new(
    Host => $self->server,
    Port  => $self->port,
    Timeout => 5,
    SSL => 1,
  );

  $smtp
    or die 'Cannot connect to ', $self->server, ':', $self->port, ' ', $@;

  $smtp->auth($self->username, $self->password)
    or die 'Could not authenticate: ' . $smtp->message;

  $smtp->mail($from)                            or die;
  $smtp->to($to)                                or die;
  $smtp->data()                                 or die;
  $smtp->datasend(encode_utf8 $mail->as_string) or die;
  $smtp->dataend()                              or die;
  $smtp->quit()                                 or die;
}

q{He signs his name to a letter he just wrote};
