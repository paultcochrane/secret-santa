use strict;
use warnings;

package Acme::SecretSanta::Dispatcher::Handle;

our $VERSION = '0';

use Moo;

has output => (
  is => 'rw',
  lazy => 1,
  default => sub { \*STDOUT },
);

with 'Acme::SecretSanta::Role::Dispatcher';

sub dispatch {
  my $fh = shift->output;
  print $fh @_;
}

q{And how does Frosty handle / All of the snow that's slowly melting away};
