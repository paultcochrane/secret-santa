use strict;
use warnings;

package Acme::SecretSanta;

our $VERSION = '0.4';

use Moo;
use MooX::HandlesVia;
use MooX::late;
use Class::Load qw( load_class );

use Acme::SecretSanta::Participant;
use Acme::SecretSanta::Group;
use Acme::SecretSanta::Types qw(
  SantaRenderer SantaSorter SantaDispatcher SantaParticipant SantaGroup
);

use Types::Standard qw( HashRef ArrayRef );
use List::Util qw( any );
use Ref::Util qw( is_plain_ref is_plain_arrayref is_blessed_ref );
use JSON::MaybeXS qw( decode_json );
use Path::Tiny qw( path );
use Carp;

has [qw( sorter renderer dispatcher is_sorted )] => (
  is => 'rw', lazy => 1,
);

has '+is_sorted' => ( default => 0 );

has '+sorter' => ( isa => SantaSorter, default => sub {
  require Acme::SecretSanta::Sorter::Shuffle;
  Acme::SecretSanta::Sorter::Shuffle->new( santa => $_[0] );
});

has '+renderer' => ( isa => SantaRenderer, default => sub {
  require Acme::SecretSanta::Renderer::String;
  Acme::SecretSanta::Renderer::String->new( santa => $_[0] );
});

has '+dispatcher' => ( isa => SantaDispatcher, default => sub {
  require Acme::SecretSanta::Dispatcher::Handle;
  Acme::SecretSanta::Dispatcher::Handle->new( santa => $_[0] );
});

foreach my $method (qw( sorter renderer dispatcher )) {
  around $method => sub {
    my $orig = shift;
    my $self = shift;
    my ($class, @rest) = @_;

    if ($class) {
      $class = ($class =~ /^\+/) ? $class =~ s/^\+//r
        : 'Acme::SecretSanta::' . ucfirst($method) . '::' . $class;

      load_class $class;
      return $self->$orig( $class->new( santa => $self, @rest ) );
    }
    else {
      return $self->$orig(@_);
    }
  };
}

sub sort     { shift->sorter->sort(@_)         }
sub render   { shift->renderer->render(@_)     }
sub dispatch { shift->dispatcher->dispatch(@_) }

has _participants => (
  is => 'ro',
  lazy => 1,
  handles_via => 'Hash',
  isa => HashRef[SantaParticipant],
  default => sub { {} },
  handles => {
    get_participant => 'get',
    add_participant => 'set',
    remove_participant => 'delete',
    total_participants => 'count',
    participants => 'values',
  },
);

around add_participant => sub {
  my $orig = shift;
  my $self = shift;

  my @participants = map { $_->can('name')
    ?  $_
    : Acme::SecretSanta::Participant->new(
        santa => $self,
        name => $_
      );
  } @_;

  $self->$orig( map { $_->name => $_ } @participants );
  $self->sorter->reset;

  return @participants;
};

around remove_participant => sub {
  my $orig = shift;
  my $self = shift;

  $self->$orig( map { $_->can('name') ? $_->name : $_; } @_ );
  $self->sorter->reset;

  return @_;
};

has _groups => (
  is => 'rw',
  lazy => 1,
  isa => HashRef[SantaGroup],
  default => sub { {} },
  handles_via => 'Hash',
  handles => {
    set_group => 'set',
    get_group => 'get',
    remove_group => 'delete',
    total_groups => 'count',
    groups => 'values',
  },
);

around set_group => sub {
  my $orig = shift;
  my $self = shift;

  my ($name, @members) = @_;

  croak 'Cannot set group: need a list of references'
    if any { !is_blessed_ref($_) } @members;

  my $group = Acme::SecretSanta::Group
      ->new( name => $name )
      ->add_member( @members );

  $self->$orig( $name => $group );
  return $group;
};

around remove_group => sub {
  my $orig = shift;
  my $self = shift;

  croak 'Cannot remove group: need a list of references'
    if any { !is_blessed_ref($_) } @_;

  $self->$orig( $_->name ) foreach @_;

  return $self;
};

sub load {
  croak 'Not enough arguments to load'
    unless scalar @_ >= 2;

  my ($class, $data, $extra) = @_;
  $extra //= {};

  unless (is_plain_ref $data) {
    $data = decode_json path($data)->slurp;
  }

  unless (is_plain_arrayref $data) {
    $data = [ $data ];
  }

  # Create a new santa object
  my $self = $class->new( $extra );

  # Add new participants
  $self->add_participant(
    map {
      Acme::SecretSanta::Participant->new( santa => $self, name => $_->{name} )
    } @{$data}
  );

  # Add exceptions
  foreach my $d (@{$data}) {
    my $participant = $self->get_participant( $d->{name} );

    $participant->add_exceptions(
      map { $self->get_participant($_) } @{$d->{exceptions}}
    );
  }

  return $self;
}

sub BUILD {
  my ($self, $arg) = @_;

  foreach my $method (qw( renderer sorter dispatcher )) {
    $self->$method->santa( $self ) unless defined $self->$method->santa;
  }
}

q{For we need a little Christmas, right this very minute};
